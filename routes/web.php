<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('login',[AuthController::class, 'login'])->name('login');
Route::get('forgot_password',[AuthController::class, 'forgotPassword'])->name('forgot.password');
Route::post('forgot_password/check_email',[AuthController::class, 'checkEmail'])->name('check.email');
Route::post('login/logon',[AuthController::class, 'logon'])->name('logon');
Route::get('logout',[AuthController::class, 'logout'])->name('logout');
Route::put('first_login',[AuthController::class, 'firstLogin'])->name('first.login');
Route::get('changePassword',[AuthController::class, 'changerPassword'])->name('change.password');
Route::put('changedPassword',[AuthController::class, 'changedPassword'])->name('changed.password');
Route::get('login/first_login',[AuthController::class, 'changedFirstLogin'])->name('form.first.login');
Route::get('dashboard/user-export',[UserController::class, 'export'])->name('export');

Route::prefix('dashboard')->middleware(['auth','first.login'])->group(function () {
    Route::get('/',[AuthController::class, 'index'])->name('dashboard');
    Route::get('user/List',[UserController::class, 'viewByUser'])->name('view.by.user');
    Route::middleware(['is.admin:0'])->group(function () {
        Route::get('dashboard/user-export',[UserController::class, 'export'])->name('export');
        Route::get('user/listSoftDelete',[UserController::class, 'listSoftDelete'])->name('user.list.soft.delete');
        Route::get('user/restore/{id}',[UserController::class, 'restore'])->name('user.restore');
        Route::delete('user/deleteOver/{id}',[UserController::class, 'deleteOver'])->name('user.delete.over');
        Route::resource('user',UserController::class);
        Route::resource('role',RoleController::class);
        Route::resource('position',PositionController::class);
        Route::resource('department',DepartmentController::class);
    }); 
});
