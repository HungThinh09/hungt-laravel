<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        @if (strpos(url()->full(), 'user'))
                            <a href="{{ route('user.create') }}">
                                <h5>
                                    <i class=" fa fa-thin fa-user"></i> <span class="action">Create User</span>
                                </h5>
                            </a>
                        @endif
                        <p>User</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('user.index') }}" class="small-box-footer">View List <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        @if (strpos(url()->full(), 'department'))
                            <a href="{{ route('department.create') }}">
                                <h5>
                                    <i class="fa fa-building"></i> <span class="action">Create Department</span>
                                </h5>
                            </a>
                        @endif
                        <p>Departments</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('department.index') }}" class="small-box-footer">View List <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        @if (strpos(url()->full(), 'position'))
                            <a href="{{ route('position.create') }}">
                                <h5>
                                    <i class="fa fa-horse"></i>  <span class="action">Create Position</span>
                                </h5>
                            </a>
                        @endif
                        <p>Positions</p> 
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{  route('position.index') }}" class="small-box-footer">View List <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        @if (strpos(url()->full(), 'role'))
                            <a href="{{ route('role.create') }}">
                                <h5>
                                    <i class="fa fa-scroll"></i>  <span class="action">Create Role</span>
                                </h5>
                            </a>
                        @endif
                        <p>Roles</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="{{  route('role.index') }}" class="small-box-footer">View List<i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
    </div><!-- /.container-fluid -->
</section>
