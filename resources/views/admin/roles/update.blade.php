@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('role.index') }}">Role</a></li>
    <li class="breadcrumb-item active">Update</li>
@endsection

@section('content')
    <form action="{{ route('role.update', $role->id) }}" method="post">
        @csrf
        @method('put')
        @include('layouts.label_form', [
            'label' => 'Role Name',
            'name' => 'name',
            'placeholder' => 'Mời bạn nhập Tên Role ...',
            'value' => $role->name,
        ])
        <button type="submit" name="addRole" class="btn btn-primary">Sửa Role</button>
    </form>
@endsection
