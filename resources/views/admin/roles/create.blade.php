@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('role.index') }}">Role</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('content')
    @include('layouts.alert')
    <form action="{{ route('role.store') }}" method="post">
        @csrf
        @include('layouts.label_form', [
            'label' => 'Role Name',
            'name' => 'name',
            'placeholder' => 'Mời bạn nhập Tên Role ...',
            'value' => old('name'),
        ])
        <button type="submit" name="addRole" class="btn btn-primary">Thêm Role</button>
    </form>
@endsection
