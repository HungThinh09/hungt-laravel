@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('department.index') }}">Department</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('content')
    @include('layouts.alert')
    <form action="{{ route('department.store') }}" method="post">
        @csrf
        @include('layouts.label_form', [
            'label' => 'Department Name',
            'name' => 'name',
            'placeholder' => 'Mời bạn nhập Tên Department ...',
            'value' => old('name'),
        ])
        <button type="submit" name="addDepartment" class="btn btn-primary">Thêm Department</button>
    </form>
@endsection
