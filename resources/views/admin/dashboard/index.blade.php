@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">Home</a></li>
@endsection


@section('content')
<div class="row col-12">
    <div class="col-md-6 offset-md-3">
        <h3>Xin chào {{ Auth::user()->role_id == 0 ? "Admin" : "User" }} <b><?= Auth::user()->name ?></b></h3>

    </div>
    <div class="card border-primary col-8 col-md-8 ">
        @include('layouts.alert')
        <div class="card-header">Thông Tin cá nhân</div>
        <div class="card-body text-primary">
            <h5 class="card-title">Full name :</h5>
            &ensp; <span class="card-text"><b><u>{{ Auth::user()->name }}</u></b></span>
        </div>
        <div class="card-body text-primary">
            <h5 class="card-title">User :</h5>
            &ensp; <span class="card-text"><b><u>{{ Auth::user()->user }}</u></b></span>
        </div>
        <div class="card-body text-primary">
            <h5 class="card-title">Giới tính : &ensp; {{ Auth::user()->gender == 0 ? "Nam": "Nữ" }}</h5>
        </div>
        <div class="card-body text-primary">
            <h5 class="card-title">Email :</h5>
            &ensp; <span class="card-text"><b><u>{{ Auth::user()->email  }}</u></b></span>
        </div>
        <div class="card-body text-primary">
            <h5 class="card-title">Tình trạng :</h5>
            &ensp; <span class="card-text">{{ Auth::user()->action == 0 ? "Đang Làm": "Đã nghỉ" }}</span>
        </div>
        <div class="card-body text-primary">
            <a href=""><i>Chi Tiết</i></a>
        </div>
    </div>

    <div class="card border-primary col-4 col-md-4">
        <div class="card-header">Image</div>
        <div class="card-body text-primary">
            <h5 class="card-title">Ảnh chân dung</h5> <br>
            <img src="{{ asset('uploads/'.(Auth::user()->gender == 0 ? "nam.png" : "nu.jpg"))}}" width="300px" alt="Image not fount">
        </div>
    </div>

</div>
@endsection
