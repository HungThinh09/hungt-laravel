@extends('layouts.main')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item active">Danh sách User</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-9 ">  
            <form action="" method="get">
                <input type="text" name="search" value="" placeholder="Nhập tên cần tìm...">
                <select style="padding: 3px" name="action">
                    <option value="" @selected(request()->status == null)>Tất cả</option>
                    <option value="0" @selected(request()->action == 0)>active</option>
                    <option value="1" @selected(request()->action == 1)>off</option>
                </select>
                <button type="submit" class="btn btn-sm btn-primary" name="">Tìm kiếm</button>
            </form> &ensp;
            
        </div>
        <div class="col-3">
            <a href="{{ route('export') }}" class="btn btn-sm btn-success">Xuất file</a>
            <a href="{{ route('user.list.soft.delete') }}" class="btn btn-sm btn-danger">Danh sách xóa mềm</a>
        </div>
        <div class="col-8"> @include('layouts.alert')</div>
        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>STT</th>
                    <th>Full Name</th>
                    <th>User</th>
                    <th>Giới tính</th>
                    <th>Phòng ban</th>
                    <th>Vị trí</th>
                    <th>Loại tài khoản</th>
                    <th>Tình trạng</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $user)
                    <tr>
                        <td>{{ ((request()->page ?? 1) - 1) * $offset + $key + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->user }}</td>
                        <td>{{ $user->gender == 0 ? 'Nam' : 'Nữ' }}</td>
                        <td>{{ $user->department->name }}</td>
                        <td>{{ $user->position->name }}</td>
                        <td>{{ $user->role->name }}</td>
                        <td>
                            <span
                                class="badge badge-{{ $user->action == 0 ? 'success' : 'danger' }}">{{ $user->action == 0 ? 'active' : 'off' }}
                            </span>
                        </td>
                        <td>
                            <div class="row">
                                <a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning"><i
                                        class="fa fa-edit"></i></a> &ensp;
                                <form action="{{ route('user.destroy', $user->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div>
            {!! $data->appends(request()->all())->links() !!}
        </div>
    </div>
@endsection
