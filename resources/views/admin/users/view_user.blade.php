@extends('layouts.main')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item active">Danh sách User</li>
@endsection

@section('content')

        <div class="col-8"> @include('layouts.alert')</div>
        <table class="table table-light">
            <tr>
                <th>STT</th>
                <th>User</th>
                <th>Giới tính</th>
                <th>Email</th>
                <th>Phòng ban</th>
                <th>Vị trí</th>
                <th>Loại tài khoản</th>
                <th>Tình trạng</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user->user }}</td>
                        <td>{{ $user->gender == 0 ? 'Nam' : 'Nữ' }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->department->name }}</td>
                        <td>{{ $user->position->name }}</td>
                        <td>{{ $user->role->name }}</td>
                        <td>
                            <span
                                class="badge badge-{{ $user->action == 0 ? 'success' : 'danger' }}">{{ $user->action == 0 ? 'active' : 'off' }}
                            </span>
                        </td>
                        <td>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

   
@endsection
