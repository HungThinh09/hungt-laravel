@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('position.index') }}">Position</a></li>
    <li class="breadcrumb-item active">Update</li>
@endsection

@section('content')
    <form action="{{ route('position.update', $position->id) }}" method="post">
        @csrf
        @method('put')
        @include('layouts.label_form', [
            'label' => 'Position Name',
            'name' => 'name',
            'placeholder' => 'Mời bạn nhập Tên Position ...',
            'value' => $position->name,
        ])
        @include('layouts.label_form', [
            'label' => 'Pority',
            'name' => 'pority',
            'placeholder' => 'Số càng nhỏ ưu tiên càng cao...',
            'value' => $position->pority,
        ])
        <button type="submit" name="editPosition" class="btn btn-primary">Sửa Position</button>
    </form>
@endsection
