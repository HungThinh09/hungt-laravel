<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $user;
    private $role;
    private $department;
    private $position;
    public function __construct(User $user , Role $role , Department $department, Position $position)
    {
        $this->user=$user;
        $this->role=$role;
        $this->department=$department;
        $this->position=$position;
    }

    public function index()
    {
        $offset = 6;
        $data= $this->user->with(['role', 'department', 'position', 'changePasswords'])->SearchUser()->ActionUser(); 
        $count = $data->count();
        $data= $data->paginate($offset);
        return view('admin.users.index',[
            'title' => 'List User',
            'topTitle' => 'Danh Sách nhân viên ('.$count.')',
            'data' => $data,
            'offset' => $offset
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles= $this->role->all();
        $positions= $this->position->all();
        $departments= $this->department->all(); 
        $data=[ 
            'roles' =>  $roles,
            'positions' => $positions, 
            'departments' =>  $departments,
        ]; 
        return view('admin.users.create',[
            'title' => 'Create User',
            'topTitle' => 'Tạo nhân viên mới',
            'data' => $data,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        if($this->user->create($request->all())) {
            return redirect(route('user.index'))->with('success','Tạo thành công user : '. $request->name);
        }
        return redirect()->back()->with('error','Tạo user không thành công ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles= $this->role->all();
        $positions= $this->position->all();
        $departments= $this->department->all(); 
        $data=[ 
            'roles' =>  $roles,
            'positions' => $positions, 
            'departments' =>  $departments,
        ]; 
       $user = $this->user->find($id);
       return view('admin.users.update',[
        'title' => 'Sửa user',
        'topTitle' => 'Sửa User',
        'user' => $user,
        'data'=> $data
       ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user= $this->user->find($id); 
        $user->update(
            $request->all()
        ); 
        if(isset($request->password)) {
            $user->update([
                'password' => $request ->password
            ]);
        }
        return redirect(route('user.index'))->with('success','Sửa thành công user : '. $request->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->find($id);
        $user->delete();
        return redirect()->back()->with('success', 'Xóa thành công user : '.$user->name);
    }
    public function listSoftDelete()
    {
        $data = $this->user->onlyTrashed()->get();
        return view('admin.users.list_delete', [
            'title' => 'Danh sách xóa mềm',
            'topTitle' => 'Danh sách xóa mềm',
            'data' => $data,
        ]);
    }
    public function restore($id)
    {
        $user = $this->user->onlyTrashed()->where('id', $id)->first();
        $user->restore();
        return redirect()->back()->with('success', 'Đã restore user : '. $user->name);
    }
    public function deleteOver($id)
    {
        $user = $this->user->onlyTrashed()->where('id', $id)->first();
        $user->ForceDelete();
        return redirect()->back()->with('success', 'Đã xóa user khỏi database ');
    }
    public function viewByUser()
    {
        $user = Auth::user();
        $users = $this->user->with(['role', 'department', 'position', 'changePasswords'])->ActionUser()
        ->join('positions','positions.id','users.position_id')
        ->where('positions.pority', '>=', $user->position->pority)
        ->where('department_id', $user->department_id)
        ->get();
       return view('admin.users.view_user',[
        'title' => 'Xem danh sách',
        'topTitle' => 'Danh sách phòng ban : '.$user->department->name.' ('.$users->count().')',
        'data' => $users
       ]);
    }
    
    public function export() 
    {
        return Excel::download(new UsersExport, 'Danh_sach_users.xlsx');
    }
}
