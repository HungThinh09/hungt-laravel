<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\AuthRequest;
use App\Http\Requests\ChangePassRequest;
use App\Http\Requests\FirstLoginRequest;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\ChangePassword;

class AuthController extends Controller
{
    private $user;
    private $changerPassword;
    public function __construct(User $user, ChangePassword $changerPassword)
    {
        $this->user= $user;
        $this->changerPassword= $changerPassword;
    }

    public function index()
    {
        if (Auth::check()) {
            return view('admin.dashboard.index', [
                'title' => 'Manager HungTHinh',
                'topTitle' => 'Dashboard',
            ]);
        }
    }

    public function login()
    {
        if(Auth::check()) {
            return redirect(route('dashboard'));
        }
        return view('auth.login', [
            'title' => "Đăng nhập hệ thống"
        ]);
    }

    public function logon(AuthRequest $request)
    {
        if (auth::attempt([
            'user' => $request->user,
            'password' => $request->password,
            'role_id' => $request->role,
        ])) {
            return redirect()->route('dashboard')->with('success, Đăng nhập thành công');
        }
        return redirect()->route('login')->with('error', 'Tài khoản hoặc password không đúng');
    }

    public function firstLogin(ChangePassRequest $request)
    {
        $this->user->find(auth::id())->update([
        'password' => $request->password,
        'first_login' => '1'
        ]);
        return redirect(route('dashboard'));
    }

    public function logout()
    {
        auth::logout();
        return redirect()->route('login');
    }

    public function forgotPassword()
    {
        return view('auth.forgot_password',[
            'title' => 'Forgot Password'
        ]);
    }

    public function checkEmail(Request $request, User $user)
    {
        $check = $user->where('email', $request->email )->first();
        if($check){
            $token = csrf_token(); 
            $check->changePasswords()->create([
                'token' => $token,
            ]); 
            $this->user->sendMail('email.changer_password',$token ,$check );
            return redirect()->back()->with('success','Check mail để thay đổi password');
        }
        return redirect()->back()->with('error', 'Email không tồn tại');
    }

    public function changerPassword(Request $request)
    {
        $item= $this->changerPassword->where('token', $request->token)->first(); 
        if($item){
          return view('auth.change_password');
        } 
        return view('errors.404');
    }

    public function changedPassword(ChangePassRequest $request)
    {
        $item= $this->changerPassword->where('token', $request->token)->first();
        $this->user->find($item->user_id)->update([
            'password' => $request->password,
        ]);
        $item->delete();
        return redirect(route('login'))->with('success','Thay đổi mật khẩu thành công');
    }

    public function changedFirstLogin()
    {
       return view('auth.first_login');
    }
}
