<?php

namespace App\Http\Controllers;

use App\Http\Requests\Position\StorePositionRequest;
use App\Http\Requests\Position\UpdatePositionRequest;
use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $position;
    public function __construct(Position $position)
    {
        $this->position= $position;
    }
    public function index()
    {
        $data = $this->position->orderby('pority','ASC')->get();
        return view('admin.positions.index',[
            'title' => 'Danh sách chức vụ',
            'topTitle' => 'Danh sách chức vụ',
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.positions.create',[
            'title' => 'Create position',
            'toptitle' => 'Tạo chức vụ',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePositionRequest $request)
    {
        if($this->position->create($request->all())) {
            return redirect(route('position.index'))->with('success', 'Tạo thành công chức vụ : '. $request->name);
        }     
        return redirect()->back()->with('error', 'Có lỗi trong quá trình tạo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = $this->position->find($id);
        return view('admin.positions.update',[
            'title' => 'Edit Position ',
            'toptitle' => 'Sửa chức vụ :'.$position->name,
            'position' => $position
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePositionRequest $request,$id)
    {
        if($this->position->find($id)->update($request->all())) {
            return redirect(route('position.index'))->with('success', 'Sửa thành công phòng ban : '. $request->name);
        }     
        return redirect()->back()->with('error', 'Có lỗi trong quá trình Sửa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $this->position->find($id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
