<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\UpdateRoleRequest;
use App\Models\Role;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $role;
    public function __construct(Role $role)
    {
        $this->role= $role;
    }
    public function index()
    {
        $data = $this->role->all();
        return view('admin.roles.index',[
            'title' => 'List Role',
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create',[
            'title' => 'Danh sách Role',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateRoleRequest $request)
    {   
        if($this->role->create($request->all())) {
            return redirect(route('role.index'))->with('success', 'Tạo thành công Role : ' . $request->name);
        }
        return redirect()->back()->with('error', "Có lỗi trong quá trình tạo");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->role->find($id); 
        return view('admin.roles.update',[
            'title' => 'Edit Role',
            'role' => $role,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        if($this->role->find($id)->update($request->all())) {
            return redirect(route('role.index'))->with('success', 'Update thành công Role : ' . $request->name);
        }
        return redirect()->back()->with('error', "Có lỗi trong quá trình Update");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = $this->role->find($id);
        $role->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
