<?php

namespace App\Http\Controllers;

use App\Http\Requests\Department\StoreDepartmentRequest;
use App\Http\Requests\Department\UpdateDepartmentRequest;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $department;
    public function __construct(Department $department)
    {
        $this->department= $department;
    }
    public function index()
    {
        $data = $this->department->all();
        return view('admin.departments.index',[
            'title' => 'Danh sách phòng ban',
            'topTitle' => 'Danh sách phòng ban',
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.departments.create',[
            'title' => 'Create Departments',
            'toptitle' => 'Tạo phòng ban',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartmentRequest $request)
    {
        if($this->department->create($request->all())) {
            return redirect(route('department.index'))->with('success', 'Tạo thành công phòng ban : '. $request->name);
        }     
        return redirect()->back()->with('error', 'Có lỗi trong quá trình tạo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = $this->department->find($id);
        return view('admin.departments.update',[
            'title' => 'Edit Departments ',
            'toptitle' => 'Sửa phòng ban :'.$department->name,
            'department' => $department
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepartmentRequest $request, $id)
    {
        if($this->department->find($id)->update($request->all())) {
            return redirect(route('department.index'))->with('success', 'Sửa thành công phòng ban : '. $request->name);
        }     
        return redirect()->back()->with('error', 'Có lỗi trong quá trình Sửa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->department->find($id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
