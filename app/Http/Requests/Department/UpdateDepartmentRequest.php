<?php

namespace App\Http\Requests\Department;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:departments,name,'.request()->id,
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên phòng ban không được bỏ trống',
            'name.unique' => 'Tên phòng ban đã tồn tại',
        ];
    }
}
